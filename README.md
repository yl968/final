# Project Title

This project includes two web services:
1. **Python FastAPI Service:** Provides machine learning model inferences.
2. **Rust Web Service:** Acts as a proxy, forwarding requests to the Python service.


## Building and Running Services
1. ## Building:
    Write the python and rust file.
2. ## Build Docker Images:
    Navigate to each service directory and build Docker images.
    ```bash
    # Build Python service
    cd python
    docker build -t python-ml-service .

    # Build Rust service
    cd ..
    docker build -t rust-web-service .
    ```
3. ## Create a Docker Network:
    ```bash
    docker network create ml-network
    ```
4. ## Run Containers:
    ```bash
    docker run --name python-fastapi --network ml-network -p 8000:8000 -d python-ml-service
    docker run --name rust-web-service --network ml-network -p 8080:8080 -d rust-web-service

    ```
## Testing Services
Use curl to test the communication between the services:
Test Python FastAPI:
```bash
curl -X POST http://localhost:8000/infer/ -H 'Content-Type: application/json' -d '{"context": "Sample context", "question": "What is this?"}'
```
Test Rust Web Service:
```bash
curl -X POST http://localhost:8080/infer -H 'Content-Type: application/json' -d '{"context": "Sample context", "question": "What is this?"}'
```
The actual test case:

![image](2209ac80a3399621bb99f5bdd1876de.png)

## AWS EKS
Deploy the python and rust project to AWS eks and test them using test file:

```bash
apiVersion: batch/v1
kind: Job
metadata:
  name: test-service-job
spec:
  template:
    spec:
      containers:
      - name: curl-container
        image: appropriate/curl
        command: ["curl"]
        args: ["-X", "POST", "http://fastapi-service.default.svc.cluster.local:80/infer/", "-H", "Content-Type: application/json", "-d", "{\"context\":\"Albert Einstein was a theoretical physicist who developed the theory of relativity, one of the two pillars of modern physics (alongside quantum mechanics). His work is also known for its influence on the philosophy of science. He is best known for his mass–energy equivalence formula E = mc^2, which has been dubbed 'the world's most famous equation'. He received the 1921 Nobel Prize in Physics 'for his services to theoretical physics, and especially for his discovery of the law of the photoelectric effect', a pivotal step in the development of quantum theory.\", \"question\":\"What is Albert Einstein best known for?\"}"]
      restartPolicy: Never
  backoffLimit: 4
```

Test the proj using:
```bash
kubectl delete job test-service-job
kubectl apply -f ./test-job.yaml
```

The actual test case:
![image](2f8250a4fc126903c98776a233e2e22.png)

Demo:https://youtu.be/itswCuzD0_Y

