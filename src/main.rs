use actix_web::{web, App, HttpServer, HttpResponse};
use serde::{Deserialize, Serialize};
use reqwest;

#[derive(Serialize, Deserialize)]
struct Query {
    context: String,
    question: String,
}

#[derive(Serialize, Deserialize)]
struct Response {
    answer: String,
}

async fn infer(query: web::Json<Query>) -> HttpResponse {
    let client = reqwest::Client::new();
    let res = client.post("http://fastapi-service.default.svc.cluster.local:80/infer/")
        .json(&*query)
        .send()
        .await
        .expect("Failed to send request");

    if res.status().is_success() {
        let response_body = res.json::<Response>().await.expect("Failed to parse response");
        HttpResponse::Ok().json(response_body)
    } else {
        HttpResponse::BadRequest().body("Inference failed")
    }
}

#[tokio::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/infer", web::post().to(infer))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
