FROM rust:latest as builder

WORKDIR /usr/src
RUN USER=root cargo new --bin Final_proj
WORKDIR /usr/src/Final_proj

# Copy the Cargo manifest files
COPY ./Cargo.toml ./Cargo.toml
COPY ./Cargo.lock ./Cargo.lock

# This trick caches the dependencies as their own layer, speeding up subsequent builds
RUN cargo build --release
RUN rm src/*.rs
RUN rm ./target/release/deps/Final_proj*

# Now copy the Rust source code into the image
COPY ./src ./src

# Build the application
RUN cargo build --release

# Start a new stage to create a smaller final image
FROM debian:bookworm-slim
ARG APP=/usr/src/app

ENV TZ=Etc/UTC \
    APP_USER=appuser

# Install necessary packages, including SSL to avoid runtime errors
RUN apt-get update && apt-get install -y \
    ca-certificates \
    tzdata \
    libssl3 \
    && rm -rf /var/lib/apt/lists/*

# Create a non-root user and switch to it
RUN groupadd $APP_USER \
    && useradd -g $APP_USER $APP_USER \
    && mkdir -p ${APP}

WORKDIR ${APP}
COPY --from=builder /usr/src/Final_proj/target/release/Final_proj ${APP}/Final_proj

# Ensure the user owns the copied files and set the executable
RUN chown -R $APP_USER:$APP_USER ${APP}
USER $APP_USER

# Expose the port used by the application
EXPOSE 8080

# Command to run the executable
CMD ["./Final_proj"]