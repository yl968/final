from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from transformers import AlbertTokenizer, AlbertForQuestionAnswering
import torch

app = FastAPI()

tokenizer = AlbertTokenizer.from_pretrained('albert-base-v2')
model = AlbertForQuestionAnswering.from_pretrained('albert-base-v2')

class Query(BaseModel):
    context: str
    question: str

@app.post("/infer/")  
async def infer(query: Query):
    inputs = tokenizer.encode_plus(query.question, query.context, add_special_tokens=True, return_tensors="pt")
    outputs = model(**inputs)
    
    answer_start_scores = outputs.start_logits
    answer_end_scores = outputs.end_logits

    answer_start = torch.argmax(answer_start_scores)
    answer_end = torch.argmax(answer_end_scores) + 1

    answer = tokenizer.decode(inputs["input_ids"][0][answer_start:answer_end], skip_special_tokens=True)
    return {"answer": answer}
